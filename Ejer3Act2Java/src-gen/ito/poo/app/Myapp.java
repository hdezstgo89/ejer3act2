package ito.poo.app;

import ito.poo.clases.Vehiculo;
import ito.poo.clases.Viajes;
import java.time.LocalDate;

public class Myapp {
	
	static  void  run () {
		Vehiculo V1 = new Vehiculo("Nissan","S1600",40,LocalDate.of(2015, 7, 5));
		System.out.println (V1);
		System.out.println ();
		Viajes Vi1 = new  Viajes("Mendoza","Av. Santos Degollado #600, Colonia Madero, 94740",LocalDate.of(2021, 4, 1),LocalDate.of(2021, 4, 10),"XBOX ONE S",150000);
		Viajes Vi2 = new  Viajes("Cordoba","Av. Guadalupe Hidalgo #16, Colonia Guadalupe, 94570",LocalDate.of(2021, 10, 15),LocalDate.of(2021, 10, 20),"Refrijeradores",200000);
		V1.addViaje (Vi1);
		V1.addViaje (Vi2);
		System.out.println (V1);
		V1.elimViaje (Vi1);
		System.out.println ();
		System.out.println (V1);
		} 
	public  static  void  main ( String [] args ) {
		run();
	}
}
