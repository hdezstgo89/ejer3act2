package ito.poo.clases;

import  java.time.LocalDate;
import  java.util.HashSet;

public class Vehiculo {

	private String registroMarca =  " ";
	private String modelo =  " ";
	private float cantidadMaxima = 0F;
	private LocalDate fechaAdquisici�n = null;
	private HashSet < Viajes > viajesRealizados = new HashSet < Viajes > ();
	
	public Vehiculo() {
		super();
	}
	public Vehiculo(String registroMarca, String modelo, float cantidadMaxima, LocalDate fechaAdquisici�n) {
		super();
		this.registroMarca = registroMarca;
		this.modelo = modelo;
		this.cantidadMaxima = cantidadMaxima;
		this.fechaAdquisici�n = fechaAdquisici�n;
	}
	/*******************************************************/
	public  boolean  addViaje (Viajes  newViaje) {
		boolean addViaje = false;
		addViaje = this.viajesRealizados.add(newViaje);
		return addViaje;
	}

	public  boolean  elimViaje ( Viajes  viaje ) {
		boolean elimViaje = false;
		elimViaje = this.viajesRealizados.remove(viaje);
		return elimViaje;
	}
	/*******************************************************/
	public String getRegistroMarca() {
		return registroMarca;
	}
	public void setRegistroMarca(String newregistroMarca) {
		this.registroMarca = newregistroMarca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String newmodelo) {
		this.modelo = newmodelo;
	}
	public float getCantidadMaxima() {
		return cantidadMaxima;
	}
	public void setCantidadMaxima(float newcantidadMaxima) {
		this.cantidadMaxima = newcantidadMaxima;
	}
	public LocalDate getFechaAdquisici�n() {
		return fechaAdquisici�n;
	}
	public void setFechaAdquisici�n(LocalDate newfechaAdquisici�n) {
		this.fechaAdquisici�n = newfechaAdquisici�n;
	}
	public HashSet<Viajes> getViajesRealizados() {
		return this.viajesRealizados;
	}
	/*******************************************************/
	@Override
	public String toString() {
		return "Vehiculo [registroMarca=" + registroMarca + ", modelo=" + modelo + ", cantidadMaxima=" + cantidadMaxima
				+ ", fechaAdquisici�n=" + fechaAdquisici�n + ", viajesRealizados=" + viajesRealizados + "]";
	}
}


