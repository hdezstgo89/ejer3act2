package ito.poo.clases;

import java.time.LocalDate;

public class Viajes {

	private  String ciudadDestino =  " ";
	private  String direccion =  " ";
	private  LocalDate fechaViaje =  null;
	private  LocalDate fechaRegreso =  null;
	private  String descripcionCarga =  " ";
	private  float montoViaje =  0F;

	 public Viajes () {
		super ();
	}
	 public Viajes(String ciudadDestino, String direccion, LocalDate fechaViaje, LocalDate fechaRegreso,String descripcionCarga, float montoViaje) {
	 super();
		this.ciudadDestino = ciudadDestino;
		this.direccion = direccion;
		this.fechaViaje = fechaViaje;
		this.fechaRegreso = fechaRegreso;
		this.descripcionCarga = descripcionCarga;
		this.montoViaje = montoViaje;
	 }
	public String getCiudadDestino() {
		return ciudadDestino;
	}
	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public LocalDate getFechaViaje() {
		return fechaViaje;
	}
	public void setFechaViaje(LocalDate fechaViaje) {
		this.fechaViaje = fechaViaje;
	}
	public LocalDate getFechaRegreso() {
		return fechaRegreso;
	}
	public void setFechaRegreso(LocalDate fechaRegreso) {
		this.fechaRegreso = fechaRegreso;
	}
	public String getDescripcionCarga() {
		return descripcionCarga;
	}
	public void setDescripcionCarga(String descripcionCarga) {
		this.descripcionCarga = descripcionCarga;
	}
	public float getMontoViaje() {
		return montoViaje;
	}
	public void setMontoViaje(float montoViaje) {
		this.montoViaje = montoViaje;
	}
	@Override
	public String toString() {
		return "Viajes [ciudadDestino=" + ciudadDestino + ", direccion=" + direccion + ", fechaViaje=" + fechaViaje
				+ ", fechaRegreso=" + fechaRegreso + ", descripcionCarga=" + descripcionCarga + ", montoViaje="
				+ montoViaje + "]";
	}
}
